import React from 'react';
import { shallow } from 'enzyme';
import Header from './header';

//setup enzyme as below
import Enzyme from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
Enzyme.configure({
    adapter: new EnzymeAdapter(),
    disableLifecycleMethods: true
});

//setup your component as below
const setUp = (props={}) => {
    const compoenentSetUp = shallow(<Header {...props}/>);
    return compoenentSetUp;
}


//start test
describe("Header Compoenent Tests", ()=>{

    let compoenent;
    beforeEach(()=> {
        compoenent = setUp();
    })

    it('should render only one header without errors', ()=>{
        const wrapperDiv = compoenent.find(`[data-test='header']`);
        expect(wrapperDiv.length).toBe(1);
    })

    it('should render a only one logo without errrors', ()=>{ 
        const logo = compoenent.find(`[data-test='logo']`);
        expect(logo.length).toBe(1);
    })
});
