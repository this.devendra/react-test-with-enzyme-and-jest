import React from 'react';
import './header.css';
import Logo from './logo.png';
const Header = (porps) => {
    return(
        <div className="header" data-test="header">
             <img className="logo" src={Logo} alt="logo" data-test="logo"/>
        </div>
    )
};

export default Header;