import React, {Component} from 'react';

export default class Headline extends Component{
    constructor(props){
        super(props);
    }
    render(){
        const {header, desc} = this.props;
        if(!header){
            return null;
        }
        return(
            <div data-test="headlineComponentTest"> 
                <h1 data-test="headerTest"> {header} </h1>
                <p data-test="descriptionTest"> {desc} </p>
            </div>
        );
    }
}