import React from 'react';
import { shallow } from 'enzyme';
import Headline from './headline';

//setup enzyme as below
import Enzyme from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import { isTSAnyKeyword, exportAllDeclaration } from '@babel/types';
Enzyme.configure({
    adapter: new EnzymeAdapter(),
    disableLifecycleMethods: true
});


//setup your component as below
const setUp = (props={}) => {
    const compoenentSetUp = shallow(<Headline {...props}/>);
    return compoenentSetUp;
}

describe("Headline Componenet", ()=>{

    describe("Have Props", ()=>{
        let wrapper;
        beforeEach(()=>{
            const props={
                header: 'Test Header',
                desc: 'Test Description'
            }

            wrapper=setUp(props);
        });

        it('should render without error', ()=>{
            const component = wrapper.find(`[data-test="headlineComponentTest"]`);
            expect(component.length).toBe(1);
        });

        it('should render H1 tag', ()=>{
            const h1Tag = wrapper.find(`[data-test="headerTest"]`);
            expect(h1Tag.length).toBe(1);
        });

        it('should render description', ()=>{
            const description = wrapper.find(`[data-test="descriptionTest"]`);
            expect(description.length).toBe(1);
        })

    });

    describe("Have no props",() =>{
        let wrapper;
        beforeEach(()=>{
            wrapper = setUp();
        });

        it('should not render', ()=>{
            const compoent = wrapper.find(`[data-test="headlineComponentTest"]`);
            expect(compoent.length).toBe(0);
        })
    });
});