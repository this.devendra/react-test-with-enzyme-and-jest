import React from 'react';
import Header from './components/header/header';
import Headline from './components/headline/headline';
import './app.css';

function App() {
  return (
    <div className="App">
        <Header />
        <section>
          <Headline header="Posts" desc="click the button to see the description"/>
        </section>
    </div>
  );
}

export default App;
